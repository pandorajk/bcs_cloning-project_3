# CLONE OF NEW YORK TIMES HOMEPAGE

# Stack:
HTML/CSS project that clones New York Time Homepage Header


# LAYOUT: 
Header and Navbar 


# Deployment:
See it at:
  https://pandorajk.gitlab.io/bcs_cloning-project_3/


# Set up and Installation: 
-git clone https://gitlab.com/Pandorajk/bcs_cloning-project_3.git
-open index.html page to see in browser. 


# Commit history: 
1. Initial commit - (16/10/2019)


# Contact:
Pandora Jane Knocker - pjknocker@yahoo.com